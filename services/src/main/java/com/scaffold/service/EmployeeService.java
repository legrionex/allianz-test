package com.scaffold.service;

import java.util.List;

import com.scaffold.bean.EmployeeBean;
import com.scaffold.service.handler.ProcessException;
import com.scaffold.service.handler.ServiceResult;

public interface EmployeeService {

	public ServiceResult<List<EmployeeBean>> getAll() throws ProcessException;

	public ServiceResult<EmployeeBean> save(EmployeeBean employeeBean) throws ProcessException;

	public ServiceResult<EmployeeBean> update(EmployeeBean employeeBean) throws ProcessException;

	public ServiceResult<EmployeeBean> delete(EmployeeBean employeeBean) throws ProcessException;

	public ServiceResult<EmployeeBean> getById(EmployeeBean employeeBean) throws ProcessException;

}
