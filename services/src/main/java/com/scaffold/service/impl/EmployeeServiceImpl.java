package com.scaffold.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.scaffold.bean.EmployeeBean;
import com.scaffold.entity.Employee;
import com.scaffold.mapper.EmployeeMapper;
import com.scaffold.repository.EmployeeRepository;
import com.scaffold.service.EmployeeService;
import com.scaffold.service.handler.ProcessException;
import com.scaffold.service.handler.ServiceResult;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;
	private EmployeeMapper mapper = Mappers.getMapper(EmployeeMapper.class);

	@Override
	public ServiceResult<List<EmployeeBean>> getAll() throws ProcessException {
		ServiceResult<List<EmployeeBean>> serviceResult = new ServiceResult<List<EmployeeBean>>();
		List<EmployeeBean> result = new ArrayList<EmployeeBean>();
		List<Employee> employees = (List<Employee>) this.employeeRepository.findAll();
		if (employees != null) {
			for (Employee employee : employees) {
				result.add(mapper.entityToBean(employee));
			}
		}
		serviceResult.setData(result);
		serviceResult.setSuccess(true);
		return serviceResult;
	}

	@Override
	@Transactional
	public ServiceResult<EmployeeBean> save(EmployeeBean employeeBean) throws ProcessException {
		ServiceResult<EmployeeBean> serviceResult = new ServiceResult<EmployeeBean>();
		Employee employee = mapper.beanToEntity(employeeBean);
		employee = this.employeeRepository.saveAndFlush(employee);
		EmployeeBean result = mapper.entityToBean(employee);
		serviceResult.setData(result);
		serviceResult.setSuccess(true);
		return serviceResult;
	}

	@Override
	@Transactional
	public ServiceResult<EmployeeBean> update(EmployeeBean employeeBean) throws ProcessException {
		ServiceResult<EmployeeBean> serviceResult = new ServiceResult<EmployeeBean>();
		Employee employee = mapper.beanToEntity(employeeBean);
		employee = this.employeeRepository.saveAndFlush(employee);
		EmployeeBean result = mapper.entityToBean(employee);
		serviceResult.setData(result);
		serviceResult.setSuccess(true);
		return serviceResult;
	}

	@Override
	@Transactional
	public ServiceResult<EmployeeBean> delete(EmployeeBean employeeBean) throws ProcessException {
		ServiceResult<EmployeeBean> serviceResult = new ServiceResult<EmployeeBean>();
		Employee employee = mapper.beanToEntity(employeeBean);
		this.employeeRepository.delete(employee);
		serviceResult.setData(null);
		serviceResult.setSuccess(true);
		return serviceResult;
	}

	@Override
	public ServiceResult<EmployeeBean> getById(EmployeeBean employeeBean) throws ProcessException {
		ServiceResult<EmployeeBean> serviceResult = new ServiceResult<EmployeeBean>();
		if (employeeBean == null || employeeBean.getId() == null) {
			throw new ProcessException("Employee Id is null!");
		}
		Employee employee = this.employeeRepository.findById(employeeBean.getId()).orElse(null);
		EmployeeBean result = mapper.entityToBean(employee);
		serviceResult.setData(result);
		serviceResult.setSuccess(true);
		return serviceResult;
	}

}
