package com.scaffold.service.impl;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.scaffold.bean.AuthenticationBean;
import com.scaffold.bean.EmployeeBean;
import com.scaffold.entity.Employee;
import com.scaffold.mapper.EmployeeMapper;
import com.scaffold.repository.EmployeeRepository;
import com.scaffold.service.AuthenticationService;
import com.scaffold.service.handler.ProcessException;
import com.scaffold.service.handler.ServiceResult;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

	@Autowired
	private EmployeeRepository employeeRepository;
	private EmployeeMapper mapper = Mappers.getMapper(EmployeeMapper.class);

	@Override
	public ServiceResult<AuthenticationBean> authenticate(AuthenticationBean authenticationBean)
			throws ProcessException {
		ServiceResult<AuthenticationBean> serviceResult = new ServiceResult<AuthenticationBean>();
		Employee employee = this.employeeRepository.findByEmployeeNoIgnoreCase(authenticationBean.getUsername());
		if (employee != null) {
			EmployeeBean employeeBean = mapper.entityToBean(employee);
			if (employeeBean.getPassword().equals(authenticationBean.getPassword())) {
				AuthenticationBean authenticationBeanR = new AuthenticationBean();
				authenticationBeanR.setPassword(employeeBean.getPassword());
				authenticationBeanR.setUsername(employeeBean.getEmployeeNo());
				serviceResult.setData(authenticationBeanR);
				serviceResult.setSuccess(true);
			} else {
				serviceResult.setMessage("Invalid username or password");
				serviceResult.setSuccess(false);
			}
		} else {
			serviceResult.setMessage("Invalid username or password");
			serviceResult.setSuccess(false);
		}
		return serviceResult;
	}

}
