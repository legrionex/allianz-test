package com.scaffold.service;

import com.scaffold.bean.AuthenticationBean;
import com.scaffold.service.handler.ProcessException;
import com.scaffold.service.handler.ServiceResult;

public interface AuthenticationService {

	public ServiceResult<AuthenticationBean> authenticate(AuthenticationBean authenticationBean) throws ProcessException;

}
