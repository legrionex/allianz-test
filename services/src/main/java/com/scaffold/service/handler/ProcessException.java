package com.scaffold.service.handler;

public class ProcessException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String message;
	private Throwable throwable;

	public ProcessException(String message) {
		this(message, null);
	}

	public ProcessException(String message, Throwable throwable) {
		this.message = message;
		this.throwable = throwable;
	}

	public String getMessageCode() {
		return message;
	}

	public Throwable getThrowable() {
		return throwable;
	}

}