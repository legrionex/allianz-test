package com.scaffold.authentication;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.scaffold.bean.AuthenticationBean;
import com.scaffold.service.AuthenticationService;
import com.scaffold.service.handler.ProcessException;
import com.scaffold.service.handler.ServiceResult;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	private AuthenticationService authenticationService;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		String username = authentication.getPrincipal().toString();
		String password = authentication.getCredentials().toString();

		AuthenticationBean authenticationBean = new AuthenticationBean();
		authenticationBean.setUsername(username);
		authenticationBean.setPassword(password);
		ServiceResult<AuthenticationBean> serviceResult = null;
		try {
			serviceResult = authenticationService.authenticate(authenticationBean);
			if (serviceResult.isSuccess()) {
				List<GrantedAuthority> userRole = new ArrayList<>();
				userRole.add(new SimpleGrantedAuthority("ROLE_USER"));
				AuthenticationBean authenticationBeanRe = serviceResult.getData();
				return new UserAuthenticationToken(authenticationBeanRe, userRole);
			} else {
				throw new UsernameNotFoundException(
						String.format("Invalid credentials", authentication.getPrincipal()));
			}
		} catch (ProcessException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}
