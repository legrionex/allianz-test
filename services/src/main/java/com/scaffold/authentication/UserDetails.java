package com.scaffold.authentication;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import com.scaffold.bean.AuthenticationBean;

public class UserDetails extends User {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private AuthenticationBean authenticationBean;

	public UserDetails(String username, String password, Collection<? extends GrantedAuthority> authorities) {
		super(username, password, authorities);
	}

	public AuthenticationBean getAuthenticationBean() {
		return authenticationBean;
	}

	public void setAuthenticationBean(AuthenticationBean authenticationBean) {
		this.authenticationBean = authenticationBean;
	}

}
