package com.scaffold.authentication;

import java.util.Collection;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import com.scaffold.bean.AuthenticationBean;

/*
 * Created by Tarn on 11 April 2017
 */
public class UserAuthenticationToken extends AbstractAuthenticationToken {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private com.scaffold.bean.AuthenticationBean authenticationBean;

	public UserAuthenticationToken(AuthenticationBean authenticationBean,
			Collection<? extends GrantedAuthority> authorities) {
		super(authorities);
		this.authenticationBean = authenticationBean;
		setAuthenticated(true);
	}

	@Override
	public Object getCredentials() {
		return null;
	}

	@Override
	public AuthenticationBean getPrincipal() {
		return authenticationBean;
	}
}