package com.scaffold.mapper;

import org.mapstruct.Mapper;

import com.scaffold.bean.EmployeeBean;
import com.scaffold.entity.Employee;

@Mapper
public interface EmployeeMapper {

	public EmployeeBean entityToBean(Employee employee);

	public Employee beanToEntity(EmployeeBean employeeBean);

}
