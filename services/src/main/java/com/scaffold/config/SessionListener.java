package com.scaffold.config;

import java.lang.reflect.Field;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.commons.lang3.time.DurationFormatUtils;
import org.springframework.session.Session;
import org.springframework.session.data.redis.RedisOperationsSessionRepository;

/*
 * Created by Tarn on 27 June 2017
 */
@WebListener
public class SessionListener implements HttpSessionListener {
	
	static RedisOperationsSessionRepository sessionRepository;
	private boolean enabled;

	public SessionListener(boolean enabled) {
		this.enabled = enabled;
	}

	@SuppressWarnings("unused")
	@Override
	public void sessionCreated(HttpSessionEvent event) {
		if (!enabled)
			return;
		String sessionId = event.getSession().getId();
		String username = getUsername(sessionId);
	}

	@SuppressWarnings("unused")
	@Override
	public void sessionDestroyed(HttpSessionEvent event) {
		if (!enabled)
			return;
		HttpSession session = event.getSession();
		String username = getUsername(session);
		long time = System.currentTimeMillis() - session.getCreationTime();
		String duration = DurationFormatUtils.formatDuration(time, "H:mm:ss");
	}

	private String getUsername(String id) {
		try {
			Session obj = sessionRepository.findById(id);
			Field field = obj.getClass().getDeclaredField("originalPrincipalName");
			field.setAccessible(true);
			return (String) field.get(obj);
		} catch (Exception e) {
			return null;
		}
	}

	private String getUsername(HttpSession session) {
		try {
			Field field = session.getClass().getDeclaredField("session");
			field.setAccessible(true);
			Object obj = field.get(session);
			field = obj.getClass().getDeclaredField("originalPrincipalName");
			field.setAccessible(true);
			return (String) field.get(obj);
		} catch (Exception e) {
			return null;
		}
	}
}
