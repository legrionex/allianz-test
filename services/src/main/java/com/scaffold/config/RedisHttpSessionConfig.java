package com.scaffold.config;

import javax.servlet.http.HttpSessionListener;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.session.web.http.HttpSessionIdResolver;

/*
 * Created by Tarn on 11 April 2017
 * https://docs.spring.io/spring-session/docs/current/reference/html5/guides/rest.html
 */
@Configuration
@EnableRedisHttpSession
public class RedisHttpSessionConfig {

	@Value("${spring.redis.host}")
	private String host;
	@Value("${spring.redis.port}")
	private String port;
	
	@Bean
	public LettuceConnectionFactory connectionFactory() {
		LettuceConnectionFactory lettuceConnectionFactory = new LettuceConnectionFactory(host, Integer.parseInt(port));
		return lettuceConnectionFactory;
	}

	@Bean
	public HttpSessionIdResolver httpSessionIdResolver() {
		return new CustomHttpSessionIdResolver();
	}

	@Bean
	public HttpSessionListener httpSessionListener() {
		return new SessionListener(true);
	}
}
