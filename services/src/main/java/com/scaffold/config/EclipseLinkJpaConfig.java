package com.scaffold.config;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.eclipse.persistence.config.PersistenceUnitProperties;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.orm.jpa.JpaBaseConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.autoconfigure.transaction.TransactionManagerCustomizers;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.vendor.AbstractJpaVendorAdapter;
import org.springframework.orm.jpa.vendor.EclipseLinkJpaVendorAdapter;
import org.springframework.transaction.jta.JtaTransactionManager;

@Configuration
public class EclipseLinkJpaConfig extends JpaBaseConfiguration {

	protected EclipseLinkJpaConfig(DataSource dataSource, JpaProperties properties,
			ObjectProvider<JtaTransactionManager> jtaTransactionManager,
			ObjectProvider<TransactionManagerCustomizers> transactionManagerCustomizers) {
		super(dataSource, properties, jtaTransactionManager, transactionManagerCustomizers);
	}

	@Override
	protected AbstractJpaVendorAdapter createJpaVendorAdapter() {
		return new EclipseLinkJpaVendorAdapter();
	}

	@Override
	protected Map<String, Object> getVendorProperties() {
		HashMap<String, Object> map = new HashMap<>();
		map.put(PersistenceUnitProperties.WEAVING, "static");
		map.put(PersistenceUnitProperties.WEAVING_LAZY, "true");
		map.put(PersistenceUnitProperties.WEAVING_INTERNAL, "true");
		map.put(PersistenceUnitProperties.TARGET_DATABASE, "org.eclipse.persistence.platform.database.H2Platform");
		map.put(PersistenceUnitProperties.DEPLOY_ON_STARTUP, "true");
		map.put(PersistenceUnitProperties.DDL_GENERATION_MODE, "database");
		map.put(PersistenceUnitProperties.DDL_GENERATION, "create-or-extend-tables");
		map.put(PersistenceUnitProperties.CACHE_STATEMENTS, "true");
		map.put(PersistenceUnitProperties.BATCH_WRITING, "JDBC");
		map.put(PersistenceUnitProperties.BATCH_WRITING_SIZE, "1000");
		map.put(PersistenceUnitProperties.CACHE_SHARED_DEFAULT, "false");
		map.put(PersistenceUnitProperties.LOGGING_LEVEL, "ALL");
		map.put(PersistenceUnitProperties.LOGGING_PARAMETERS, "true");
		map.put(PersistenceUnitProperties.PERSISTENCE_CONTEXT_CLOSE_ON_COMMIT, "true");
		map.put(PersistenceUnitProperties.PERSISTENCE_CONTEXT_FLUSH_MODE, "COMMIT");
		map.put(PersistenceUnitProperties.PERSISTENCE_CONTEXT_PERSIST_ON_COMMIT, "false");
		map.put(PersistenceUnitProperties.CONNECTION_POOL_INITIAL, "1");
		map.put(PersistenceUnitProperties.CONNECTION_POOL_MIN, "64");
		map.put(PersistenceUnitProperties.CONNECTION_POOL_MAX, "64");
		return map;
	}

}
