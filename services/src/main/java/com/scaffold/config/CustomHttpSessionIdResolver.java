package com.scaffold.config;

import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.session.web.http.HeaderHttpSessionIdResolver;
import org.springframework.util.StringUtils;

/*
 * Created by Tarn on 26 April 2017
 */
class CustomHttpSessionIdResolver extends HeaderHttpSessionIdResolver {
	private static final String ParameterName = "token";

	CustomHttpSessionIdResolver() {
		super("X-Auth-Token");
	}

	@Override
	public List<String> resolveSessionIds(HttpServletRequest request) {
		List<String> list = super.resolveSessionIds(request);
		if (list.isEmpty()) {
			String value = request.getParameter(ParameterName);
			if (StringUtils.hasText(value))
				list = Collections.singletonList(value);
		}
		return list;
	}
}