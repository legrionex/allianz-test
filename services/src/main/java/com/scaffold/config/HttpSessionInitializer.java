package com.scaffold.config;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.session.data.redis.RedisOperationsSessionRepository;
import org.springframework.session.web.context.AbstractHttpSessionApplicationInitializer;
import org.springframework.stereotype.Component;

/*
 * Created by Tarn on 11 April 2017
 */
@Component
public class HttpSessionInitializer extends AbstractHttpSessionApplicationInitializer {

	@Value("${spring.session.timeout}")
	private String timeout;
	@Value("${spring.session.redis.namespace}")
	private String namespace;
	@Autowired
	private RedisOperationsSessionRepository sessionRepository;

	@PostConstruct
	public void init() {
		Integer to = 1800;
		if (timeout != null) {
			to = Integer.parseInt(timeout);
		}
		sessionRepository.setDefaultMaxInactiveInterval(to);
		sessionRepository.setRedisKeyNamespace(namespace);
		SessionListener.sessionRepository = sessionRepository;
	}
}
