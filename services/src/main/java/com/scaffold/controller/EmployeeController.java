package com.scaffold.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.scaffold.bean.EmployeeBean;
import com.scaffold.service.EmployeeService;
import com.scaffold.service.handler.ProcessException;
import com.scaffold.service.handler.ServiceResult;

import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping("employee")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

	@PostMapping(value = "/get-by-Id", produces = MediaType.APPLICATION_JSON_VALUE)
	@Operation(summary = "API used for get employee by id")
	public ServiceResult<EmployeeBean> getById(@RequestBody EmployeeBean employeeBean) throws ProcessException {
		return employeeService.getById(employeeBean);
	}

	@GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
	@Operation(summary = "API used for list employee")
	public ServiceResult<List<EmployeeBean>> list() throws ProcessException {
		return employeeService.getAll();
	}

	@PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
	@Operation(summary = "API used for save employee")
	public ServiceResult<EmployeeBean> save(@RequestBody EmployeeBean employeeBean) throws ProcessException {
		return employeeService.save(employeeBean);
	}

	@PostMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
	@Operation(summary = "API used for save employee")
	public ServiceResult<EmployeeBean> update(@RequestBody EmployeeBean employeeBean) throws ProcessException {
		return employeeService.update(employeeBean);
	}

	@PostMapping(value = "/delete", produces = MediaType.APPLICATION_JSON_VALUE)
	@Operation(summary = "API used for save employee")
	public ServiceResult<EmployeeBean> delete(@RequestBody EmployeeBean employeeBean) throws ProcessException {
		return employeeService.delete(employeeBean);
	}

}
