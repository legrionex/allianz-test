package com.scaffold.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.scaffold.bean.EmployeeBean;
import com.scaffold.service.handler.ProcessException;
import com.scaffold.service.handler.ServiceResult;
import com.scaffold.service.impl.EmployeeServiceImpl;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeServiceImplTest {

	@TestConfiguration
	static class EmployeeServiceImplTestContextConfiguration {

		@Bean
		public EmployeeService employeeService() {
			return new EmployeeServiceImpl();
		}
	}

	@Autowired
	private EmployeeService employeeService;

	@Test
	public void testGetAll() {
		try {
			ServiceResult<List<EmployeeBean>> serviceResult = employeeService.getAll();
			assertTrue(serviceResult.isSuccess());
			assertNotNull(serviceResult.getData());
			assertTrue(serviceResult.getData().size() > 0);
		} catch (ProcessException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testSave() {
		EmployeeBean employeeBean = new EmployeeBean();
		employeeBean.setEmail("testcreate@test.com");
		employeeBean.setEmployeeNo("b1234");
		employeeBean.setMobileNo("0123456789");
		employeeBean.setName("TEST CRATE");
		employeeBean.setPassword("test");

		ServiceResult<EmployeeBean> serviceResult = null;
		try {
			serviceResult = employeeService.save(employeeBean);
		} catch (ProcessException e) {
			e.printStackTrace();
		}
		assertTrue(serviceResult.isSuccess());
		assertNotNull(serviceResult.getData());
		assertNotNull(serviceResult.getData().getId());

		ServiceResult<EmployeeBean> serviceResultFind = null;
		try {
			serviceResultFind = employeeService.getById(serviceResult.getData());
		} catch (ProcessException e) {
			e.printStackTrace();
		}
		assertTrue(serviceResultFind.isSuccess());
		assertNotNull(serviceResultFind.getData());
		assertNotNull(serviceResultFind.getData().getId());

		assertTrue(serviceResult.getData().getId().intValue() == serviceResultFind.getData().getId().intValue());
	}

	@Test
	public void testUpdate() {
		EmployeeBean employeeBean = new EmployeeBean();
		employeeBean.setEmail("testupdate@test.com");
		employeeBean.setEmployeeNo("b1234");
		employeeBean.setMobileNo("0123456789");
		employeeBean.setName("TEST UPDATE");
		employeeBean.setPassword("test");

		ServiceResult<EmployeeBean> serviceResult = null;
		try {
			serviceResult = employeeService.save(employeeBean);
		} catch (ProcessException e) {
			e.printStackTrace();
		}

		assertTrue(serviceResult.isSuccess());
		assertNotNull(serviceResult.getData());
		assertNotNull(serviceResult.getData().getId());

		EmployeeBean employeeBeanU = serviceResult.getData();
		employeeBeanU.setEmail("testupdate2@test.com");
		employeeBeanU.setEmployeeNo("b12345");
		employeeBeanU.setMobileNo("0123455555");
		employeeBeanU.setName("TEST UPDATE 2");
		employeeBeanU.setPassword("test2");

		ServiceResult<EmployeeBean> serviceResultFind = null;
		try {
			serviceResultFind = employeeService.update(employeeBeanU);
		} catch (ProcessException e) {
			e.printStackTrace();
		}
		assertTrue(serviceResultFind.isSuccess());
		assertNotNull(serviceResultFind.getData());
		assertNotNull(serviceResultFind.getData().getId());

		assertTrue(serviceResult.getData().getId().intValue() == serviceResultFind.getData().getId().intValue());
		assertTrue(!employeeBean.getEmail().equals(serviceResultFind.getData().getEmail()));
		assertTrue(!employeeBean.getEmployeeNo().equals(serviceResultFind.getData().getEmployeeNo()));
		assertTrue(!employeeBean.getMobileNo().equals(serviceResultFind.getData().getMobileNo()));
		assertTrue(!employeeBean.getName().equals(serviceResultFind.getData().getName()));
		assertTrue(!employeeBean.getPassword().equals(serviceResultFind.getData().getPassword()));
	}

	@Test
	public void testDelete() {
		EmployeeBean employeeBean = new EmployeeBean();
		employeeBean.setEmail("testdelete@test.com");
		employeeBean.setEmployeeNo("b12345");
		employeeBean.setMobileNo("0123455555");
		employeeBean.setName("TEST DELETE");
		employeeBean.setPassword("test");

		ServiceResult<EmployeeBean> serviceResult = null;
		try {
			serviceResult = employeeService.save(employeeBean);
		} catch (ProcessException e) {
			e.printStackTrace();
		}

		assertTrue(serviceResult.isSuccess());
		assertNotNull(serviceResult.getData());
		assertNotNull(serviceResult.getData().getId());

		ServiceResult<EmployeeBean> serviceResultDelete = null;
		try {
			serviceResultDelete = employeeService.delete(serviceResult.getData());
		} catch (ProcessException e) {
			e.printStackTrace();
		}
		assertTrue(serviceResultDelete.isSuccess());

		ServiceResult<EmployeeBean> serviceResultFind = null;
		try {
			serviceResultFind = employeeService.getById(serviceResult.getData());
		} catch (ProcessException e) {
			e.printStackTrace();
		}
		assertTrue(serviceResultFind.isSuccess());
		assertNull(serviceResultFind.getData());
	}

	@Test
	public void testGetById() {
		EmployeeBean employeeBean = new EmployeeBean();
		employeeBean.setId(1);
		ServiceResult<EmployeeBean> serviceResultFind = null;
		try {
			serviceResultFind = employeeService.getById(employeeBean);
		} catch (ProcessException e) {
			e.printStackTrace();
		}
		assertTrue(serviceResultFind.isSuccess());
		assertNotNull(serviceResultFind.getData());
		assertNotNull(serviceResultFind.getData().getId());
		assertNotNull(serviceResultFind.getData().getEmail());
		assertNotNull(serviceResultFind.getData().getEmployeeNo());
		assertNotNull(serviceResultFind.getData().getMobileNo());
		assertNotNull(serviceResultFind.getData().getName());
		assertNotNull(serviceResultFind.getData().getPassword());
	}

}
