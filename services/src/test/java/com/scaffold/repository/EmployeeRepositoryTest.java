package com.scaffold.repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.scaffold.entity.Employee;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeRepositoryTest {
 
    @Autowired
    private EmployeeRepository employeeRepository;
    
    @Test
    public void testFindByEmployeeNoIgnoreCase() {
    	Employee employee1 = employeeRepository.findByEmployeeNoIgnoreCase("a001");
    	assertNotNull(employee1);
    	Employee employee3 = employeeRepository.findByEmployeeNoIgnoreCase("a003");
    	assertNotNull(employee3);
    	
    	Employee employee6 = employeeRepository.findByEmployeeNoIgnoreCase("a006");
    	assertNull(employee6);
    	
    }
	
}
