import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, Router } from '@angular/router';

@Injectable()
export class Guard implements CanActivate, CanActivateChild {
  constructor(private router: Router) { }

  canActivate() {
    this.router.navigate(['/countrys']);
    return true;
  }

  canActivateChild() {
    this.router.navigate(['/countrys']);
    return true;
  }
}
