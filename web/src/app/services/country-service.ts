import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { Country } from '../domain/country';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CountryService {

    constructor(private service: ApiService) { }

    getCountrys(): any {
        let urlStr: string = '/api/v2/all';
        return this.service.get(urlStr);
    }
}
