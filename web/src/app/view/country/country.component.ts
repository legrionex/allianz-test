import { Component } from '@angular/core';
import { CountryService } from '../../services/country-service';
import { MessageService, Message } from 'primeng/api';
import { Country } from '../../domain/country';

@Component({
   selector: 'app-country',
   templateUrl: './country.component.html',
   styleUrls: ['./country.component.scss'],
   providers: [CountryService, MessageService]
})
export class CountryComponent {

   blocked: boolean;
   countrys: Country[];

   constructor(private countryService: CountryService, private messageService: MessageService) {
   }

   ngOnInit() {

      this.blocked = true;
      this.countryService.getCountrys()
         .subscribe(result => {
            this.countrys = result;
            this.blocked = false;
         }, () => {
            // TODO handle exception
            this.blocked = false;
         });



   }


}
