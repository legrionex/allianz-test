import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

export class Error {
   readonly status: number;
   readonly message: string;

   constructor(status: number, message: string) {
      this.status = status;
      this.message = message;
   }

   toString(): string {
      return this.message;
   }
}

@Injectable({
   providedIn: 'root'
})

export class ApiService {

   // public url = environment.serviceUrl;

   constructor(private http: HttpClient) {
   }

   private createHttpOptions(options?: any, json?: boolean): any {
      if (options && options.headers) {
         return options;
      }
      let headers = new HttpHeaders();
      headers.append("Access-Control-Allow-Origin", "*");
      if (json !== false) {
         headers = headers.append('Content-Type', 'application/json;charset=utf-8');
      }
      if (options) {
         options.headers = headers;
      } else {
         options = { headers };
      }
      return options;
   }

   get(api: string, options?: any): Observable<any> {
      return this.http.get(api, this.createHttpOptions(options))
         .pipe(catchError(this.handleError(api)));
   }

   post(api: string, body?: any, options?: any): Observable<any> {
      let json = false;
      if ((body instanceof Object) && !(body instanceof FormData)) {
         body = JSON.stringify(body);
         json = true;
      }
      return this.http.post(api, body, this.createHttpOptions(options, json))
         .pipe(catchError(this.handleError(api)));
   }

   private handleError(api: string) {
      return (error: any): Observable<any> => {
         let alert = true;
         let status = 0;
         let message: string;
         if (error instanceof HttpErrorResponse) {
            status = error.status;
            if (error.error) {
               if (error.error instanceof Blob) {
                  const url = URL.createObjectURL(error.error);
                  const xhr = new XMLHttpRequest();
                  xhr.open('GET', url, false);
                  xhr.send();
                  URL.revokeObjectURL(url);
                  message = JSON.parse(xhr.responseText).message;
               } else if (error.error instanceof Object) {
                  message = error.error.message;
               } else {
                  message = error.error;
               }
            }
            if (!message) {
               message = error.message || error.toString();
            }
            message = 'HTTP ' + error.status + '\n' + message;
            // console.error(`${error.status} ${message}`);
         } else {
            console.error(error.status + ' ' + error);
            message = error.message || error;
         }
         if (alert) {
            // this.toast.error(message);
         }
         return throwError(new Error(status, message));
      };
   }

}
