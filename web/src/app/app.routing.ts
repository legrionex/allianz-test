import { Routes, RouterModule, Router } from '@angular/router';
import { NgModule } from '@angular/core'

import { Guard } from './guard/guard.service';

import { AppComponent } from './app.component';
import { DispatcherComponent } from './dispatcher.component';
import { CountryComponent } from '../app/view/country/country.component';

export const AppRoutes: Routes = [

  {
    path: '',
    component: AppComponent,
    children: [
      { path: '', redirectTo: '/countrys', pathMatch: 'full' },
      { path: 'countrys', component: CountryComponent }, 
      { path: '**', component: DispatcherComponent, canActivate: [Guard] }
    ]
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(AppRoutes, { useHash: true })],
  exports: [RouterModule]
})

export class AppRoutingModule { }
