import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { ToastModule } from 'primeng/toast';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { BlockUIModule } from 'primeng/blockui';
import { CardModule } from 'primeng/card';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { OrderListModule } from 'primeng/orderlist';

import { Guard } from './guard/guard.service';
import { AppComponent } from './app.component';
import { DispatcherComponent } from './dispatcher.component';
import { AppRoutingModule } from './app.routing';
import { CountryComponent } from '../app/view/country/country.component';

import { LocationStrategy, PathLocationStrategy } from '@angular/common';

@NgModule({
    declarations: [
        AppComponent,
        DispatcherComponent,
        CountryComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        TableModule,
        HttpClientModule,
        InputTextModule,
        DialogModule,
        ButtonModule,
        ToastModule,
        MessagesModule,
        MessageModule,
        BlockUIModule,
        ProgressSpinnerModule,
        CardModule,
        OrderListModule,
        AppRoutingModule
    ],
    providers: [Guard, {
        provide: [LocationStrategy],
        useClass: PathLocationStrategy,
    }],
    bootstrap: [AppComponent]
})
export class AppModule { }
